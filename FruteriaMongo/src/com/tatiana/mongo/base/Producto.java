package com.tatiana.mongo.base;

import org.bson.types.ObjectId;

public class Producto  {
    private ObjectId id;
    private String nombre;
    private String origen;
    private float precio;

    public  Producto(){

    }

    public Producto(String nombre, String origen, float precio) {
        this.nombre= nombre;
        this.origen= origen;
        this.precio= precio;
    }


    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    @Override
    public String toString() {
        return nombre + " - " + origen + "% - " + precio + " €";
    }
}

