package com.tatiana.mongo.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Empleado {
    private ObjectId id;
    private String nombre;
    private String apellidos;
    private String cod_empleado;
    private LocalDate fecha_nacimiento;

    public Empleado(String nombre, String apellidos, String cod_empleado, LocalDate fecha_nacimiento){
        this.nombre= nombre;
        this.apellidos= apellidos;
        this.cod_empleado= cod_empleado;
        this.fecha_nacimiento= fecha_nacimiento;
    }
    public  Empleado(){

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCod_empleado() {
        return cod_empleado;
    }

    public void setCod_empleado(String cod_empleado) {
        this.cod_empleado = cod_empleado;
    }

    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    @Override
    public String toString() {
        return nombre + " " + apellidos;
    }
}
