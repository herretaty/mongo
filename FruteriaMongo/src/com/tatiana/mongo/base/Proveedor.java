package com.tatiana.mongo.base;

import org.bson.types.ObjectId;

public class Proveedor {
    private ObjectId id;
    private String proveedor;

    public Proveedor(String proveedor){
        this.proveedor= proveedor;
    }

    public Proveedor(){

    }

    public ObjectId getId() {
        return this.id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    @Override
    public String toString() {
        return proveedor;
    }


}
