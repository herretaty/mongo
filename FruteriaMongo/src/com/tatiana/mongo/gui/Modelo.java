package com.tatiana.mongo.gui;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.tatiana.mongo.base.Empleado;
import com.tatiana.mongo.base.Producto;
import com.tatiana.mongo.base.Proveedor;
import org.bson.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase modelo
 */
public class Modelo {
    private MongoClient cliente;
    private MongoCollection<Document> productos;
    private MongoCollection<Document> empleados;
    private MongoCollection<Document> proveedores;

    /**
     * Metodo que se utiliza para conectar con la base de datos de Mongo
     */
    public void conectar() {
        cliente= new MongoClient();
        String DATABASE= "Fruteria";
        MongoDatabase db= cliente.getDatabase(DATABASE);

        String COLECCION_PRODUCTOS = "Productos";
        productos= db.getCollection(COLECCION_PRODUCTOS);
        String COLECCION_EMPLEADOS="Empleados";
        empleados= db.getCollection(COLECCION_EMPLEADOS);
        String COLECCION_PROVEEDORES="Proveedores";
        proveedores= db.getCollection(COLECCION_PROVEEDORES);

    }

    /**
     * Metodo para desconectar de la base de datos
     */
    public void desconectar(){
        cliente.close();
        cliente=null;

    }

    /**
     * Metodo que nos devolvera la conexion como cliente
     * @return
     */
    public MongoClient getCliente() {
        return cliente;
    }

    /**
     * Metodo para listar los productos
     * @return
     */
    public ArrayList<Producto> getProductos() {
        ArrayList<Producto> lista = new ArrayList<>();

        for (Document document : productos.find()) {
            lista.add(documentToProducto(document));
        }
        return lista;
    }

    /**
     * Metodo que lista los productos en dos criterios regulares
     * @param comparador
     * @return
     */
    public ArrayList<Producto>getProductos(String comparador){
        ArrayList<Producto> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : productos.find(query)) {
            lista.add(documentToProducto(document));
        }
        return lista;

    }

    /**
     * Metodo que lista los empleados
     * @return
     */

    public ArrayList<Empleado> getEmpleados() {
        ArrayList<Empleado> lista = new ArrayList<>();

        for (Document document : empleados.find()) {
            lista.add(documentToEmpleado(document));
        }
        return lista;
    }

    /**
     * Metodo que lista los empleados con dos criterios regulares
     * @param comparador
     * @return
     */
   public ArrayList<Empleado>getEmpleados(String comparador){
        ArrayList<Empleado> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("apellidos", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : empleados.find(query)) {
            lista.add(documentToEmpleado(document));
        }

        return lista;
    }

    /**
     * Metodo que lista los proveedores
     * @return
     */
    public ArrayList<Proveedor>getProveedores(){
        ArrayList<Proveedor> lista= new ArrayList<>();

        for (Document document : proveedores.find()){
            lista.add(documentToProveedor(document));
        }
        return lista;
    }

    /**
     * Metodo que lista los proveedores con dos criterios regulares
     * @param comparador
     * @return
     */

    public ArrayList<Proveedor> getProveedores(String comparador){
        ArrayList<Proveedor> lista= new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios= new ArrayList<>();

        listaCriterios.add(new Document("Proveedor", new Document("$regex", "/*" + comparador + "/*")));
        query.append(String.valueOf(listaCriterios),("$or"));

        for (Document document : proveedores.find(query)){
            lista.add(documentToProveedor(document));
        }
        return lista;
    }

    /**
     * Metodo que nos va a guardar los datos en la base de datos de
     * producto
     * empleados
     * proveedores
     * @param obj
     */
    public void guardarObjeto(Object obj) {
        if (obj instanceof Producto) {
            productos.insertOne(objectToDocument(obj));
        } else if (obj instanceof Empleado) {
            empleados.insertOne(objectToDocument(obj));
        } else if (obj instanceof Proveedor){
            proveedores.insertOne(objectToDocument(obj));
        }
    }

    /**
     * Metodo que nos modificara los datos que queramos cambiar en la base de datos
     * @param obj
     */

    public void modificarObjeto(Object obj) {
        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;
            productos.replaceOne(new Document("_id", producto.getId()), objectToDocument(producto));
        } else if (obj instanceof Empleado) {
            Empleado empleado = (Empleado) obj;
            empleados.replaceOne(new Document("_id", empleado.getId()), objectToDocument(empleado));
        }else if (obj instanceof  Proveedor){
            Proveedor proveedor= (Proveedor) obj;
            proveedores.replaceOne(new Document("_id", proveedor.getId()), objectToDocument(proveedor));
        }
    }

    /**
     * Metodo que eliminara los datos seleccionados de la base de datos
     * @param obj
     */
    public void eliminarObjeto(Object obj) {
        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;
            productos.deleteOne(objectToDocument(producto));
        } else if (obj instanceof Empleado) {
            Empleado empleado = (Empleado) obj;
            empleados.deleteOne(objectToDocument(empleado));
        }else if (obj instanceof Proveedor){
            Proveedor proveedor= (Proveedor) obj;
            proveedores.deleteOne(objectToDocument(proveedor));
        }
    }

    /**
     * Metodo que registra todas las filas del proveedor
     * @param document
     * @return
     */
    private Proveedor documentToProveedor(Document document) {
        Proveedor proveedor= new Proveedor();

        proveedor.setId(document.getObjectId("_id"));
        proveedor.setProveedor(document.getString("proveedor"));
        return proveedor;
    }

    /**
     * Metodo que registra todas las filas del empleado
     * @param document
     * @return
     */

    private Empleado documentToEmpleado(Document document) {
        Empleado empleado = new Empleado();

        empleado.setId(document.getObjectId("_id"));
        empleado.setNombre(document.getString("nombre"));
        empleado.setApellidos(document.getString("apellidos"));
        empleado.setFecha_nacimiento(LocalDate.parse(document.getString("fecha_nacimiento")));
        return empleado;
    }

    /**
     * Metodo que registra todas las filas del producto
     * @param document
     * @return
     */

    private Producto documentToProducto(Document document) {
        Producto producto = new Producto();

        producto.setId(document.getObjectId("_id"));
        producto.setNombre(document.getString("nombre"));
        producto.setOrigen(document.getString("origen"));
        producto.setPrecio((Float.parseFloat(String.valueOf(document.getDouble("precio")))));
        return producto;
    }

    /**
     * Metodo que lista todos los objetos
     * @param obj
     * @return
     */
    private Document objectToDocument(Object obj) {
        Document document = new Document();

        if (obj instanceof Producto) {
            Producto producto = (Producto) obj;

            document.append("nombre", producto.getNombre());
            document.append("origen", producto.getOrigen());
            document.append("precio", producto.getPrecio());
        } else if (obj instanceof Empleado){
            Empleado empleado = (Empleado) obj;

            document.append("nombre", empleado.getNombre());
            document.append("apellidos", empleado.getApellidos());
            document.append("cod_empleado",empleado.getCod_empleado());
            document.append("fecha_nacimiento",empleado.getFecha_nacimiento().toString());
        }else if (obj instanceof Proveedor){
            Proveedor proveedor= (Proveedor) obj;
            document.append("proveedor", proveedor.getProveedor());
        }else {
            return null;
        }
        return document;
    }


}





