package com.tatiana.mongo.gui;

import com.tatiana.mongo.base.Empleado;
import com.tatiana.mongo.base.Producto;
import com.tatiana.mongo.base.Proveedor;
import com.tatiana.mongo.util.Util;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * @author tatiana
 */
public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    private Modelo modelo;
    private Vista vista;

    /**
     * Constructores
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.modelo= modelo;
        this.vista= vista;

        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);

        try {
            modelo.conectar();
            vista.itemConectar.setText("Desconectar");
            vista.setTitle("Fruteria - <CONECTADO>");
            setBotonesActivados(true);
            listarProductos();
            listarEmpleados();
            listarProveedores();
        }catch (Exception ex){
            Util.mostrarMensajeError("Imposible conectar con el servidor.");

        }

    }

    /**
     * Metodo para listar los proveedores
     */
    private void listarProveedores() {
        vista.dlmProveedores.clear();
        for (Proveedor proveedor: modelo.getProveedores()){
            vista.dlmProveedores.addElement(proveedor);
        }
    }

    /**
     * Metodo para listar los empleados
     */
    private void listarEmpleados() {
        vista.dlmEmpleados.clear();
        for (Empleado empleado : modelo.getEmpleados()) {
            vista.dlmEmpleados.addElement(empleado);
        }
    }

    /**
     * Metodo para lostar los productos
     */
    private void listarProductos() {
        vista.dlmProductos.clear();
        for (Producto producto : modelo.getProductos()) {
            vista.dlmProductos.addElement(producto);
        }
    }

    /**
     * Metodo para controlas los botones
     * @param activados
     */

    private void setBotonesActivados(boolean activados) {
        vista.btnAnnadirProducto.setEnabled(activados);
        vista.btnModificarProducto.setEnabled(activados);
        vista.btnEliminarProducto.setEnabled(activados);

        vista.btnAnnadirEmpleado.setEnabled(activados);
        vista.btnModificarEmpleado.setEnabled(activados);
        vista.btnEliminarEmpleado.setEnabled(activados);

        vista.btnAnnadirProveedor.setEnabled(activados);
        vista.btnModificarProveedor.setEnabled(activados);
        vista.btnEliminarProveedor.setEnabled(activados);
    }

    /**
     * Metodo para controlar los listeners de las listas
     * @param listener
     */
    private void addListSelectionListeners(ActionListener listener) {
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.listProveedores.addListSelectionListener((ListSelectionListener) listener);


    }

    /**
     * Metodo para controlar las busquedas
     * @param listener
     */
    private void addKeyListeners(KeyListener listener) {
        vista.txtBuscarProducto.addKeyListener(listener);
        vista.txtBuscarEmpleado.addKeyListener(listener);
        vista.txtBuscarProveedor.addKeyListener(listener);
    }

    /**
     * Metodo para controlar los botones que vamos a añadir
     * @param listener
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnAnnadirProducto.addActionListener(listener);
        vista.btnModificarProducto.addActionListener(listener);
        vista.btnEliminarProducto.addActionListener(listener);

        vista.btnAnnadirEmpleado.addActionListener(listener);
        vista.btnModificarEmpleado.addActionListener(listener);
        vista.btnEliminarEmpleado.addActionListener(listener);

        vista.btnAnnadirProveedor.addActionListener(listener);
        vista.btnModificarProveedor.addActionListener(listener);
        vista.btnEliminarProveedor.addActionListener(listener);

        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);

    }

    /**
     * Metodo para asignar los botones, y que funcionen correctamente
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()){
            case"conexion":
                try {
                    if (modelo.getCliente()== null){
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        vista.setTitle("Fruteria - <CONECTADO>");
                        setBotonesActivados(true);
                        listarProductos();
                        listarEmpleados();
                        listarProveedores();
                    }else {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        vista.setTitle("Fruteria - <SIN CONEXION>");
                        setBotonesActivados(false);
                        vista.dlmProductos.clear();
                        vista.dlmEmpleados.clear();
                        vista.dlmProveedores.clear();
                        limpiarCamposProducto();
                        limpiarCamposEmpleado();
                        limpiarCamposProveedor();
                    }
                }catch (Exception ex){
                    Util.mostrarMensajeError("Imposible establecer conexión con el servidor.");
                }
                break;
            case "salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Añadir producto":
                if (comprobarCamposProducto()){
                    modelo.guardarObjeto(new Producto(vista.txtNombreProducto.getText(),
                    vista.txtOrigenProducto.getText(),
                    Float.parseFloat(vista.txtPrecioProducto.getText())));
                    limpiarCamposProducto();
                }else {Util.mostrarMensajeError("No ha sido posible insertar el producto en la base de datos.\n" +
                        "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarProductos();
                break;
            case "Modificar producto":
                if (vista.listProductos.getSelectedValue() !=null){
                   if (comprobarCamposProducto()){
                       Producto producto = vista.listProductos.getSelectedValue();
                       producto.setNombre(vista.txtNombreProducto.getText());
                       producto.setOrigen(vista.txtOrigenProducto.getText());
                       producto.setPrecio(Float.parseFloat(vista.txtPrecioProducto.getText()));
                       modelo.modificarObjeto(producto);
                       limpiarCamposProducto();
                   }else {
                       Util.mostrarMensajeError("No ha sido posible modificar el producto en la base de datos.\n" +
                               "Compruebe que los campos contengan el tipo de dato requerido.");
                   }
                    listarProductos();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;
            case "Eliminar producto":
                if (vista.listProductos.getSelectedValue() != null) {
                    modelo.eliminarObjeto(vista.listProductos.getSelectedValue());
                    listarProductos();
                    limpiarCamposProducto();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;
            case "Añadir empleado":
                if (comprobarCamposEmpleado()){
                    modelo.guardarObjeto(new Empleado(vista.txtNombreEmpleado.getText(),
                            vista.txtCodigoEmpleado.getText(),
                            vista.txtApellidosEmpleado.getText(),
                            vista.fecha_nacimiento.getDate()));
                    limpiarCamposEmpleado();
                }
                else {
                    Util.mostrarMensajeError("No ha sido posible insertar el empleado .\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarEmpleados();
                break;
            case "Modificar empleado":
               if (vista.listEmpleados.getSelectedValue()!=null){
                if (comprobarCamposEmpleado()){
                    Empleado empleado = vista.listEmpleados.getSelectedValue();
                    empleado.setNombre(vista.txtNombreEmpleado.getText());
                    empleado.setApellidos(vista.txtApellidosEmpleado.getText());
                    empleado.setCod_empleado(vista.txtCodigoEmpleado.getText());
                    empleado.setFecha_nacimiento(vista.fecha_nacimiento.getDate());
                    modelo.modificarObjeto(empleado);
                    limpiarCamposEmpleado();
                }else {
                    Util.mostrarMensajeError("No ha sido posible modificar el empleado .\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                   listarEmpleados();
               } else {
                   Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
               }
                break;
            case"Eliminar empleado":
                if (vista.listEmpleados.getSelectedValue()!=null){
                    modelo.eliminarObjeto(vista.listEmpleados.getSelectedValue());
                    listarEmpleados();
                    limpiarCamposEmpleado();
                }else {
                    Util.mostrarMensajeError("No hay ningun elemento seleccionado");
                }

                break;
            case"Añadir proveedor":
                if (comprobarCamposProveedor()){
                    modelo.guardarObjeto(new Proveedor((vista.txtProveedor.getText())));
                    limpiarCamposProveedor();
                }else {
                    Util.mostrarMensajeError("No ha sido posible insertar el departamento en la base de datos.\n" +
                            "Compruebe que los campos contengan el tipo de dato requerido.");
                }
                listarProveedores();
                break;
            case"Modificar proveedor":
                if (vista.listProveedores.getSelectedValue()!=null){
                    if (comprobarCamposProveedor()){
                        Proveedor proveedor= vista.listProveedores.getSelectedValue();
                        proveedor.setProveedor(vista.txtProveedor.getText());
                        modelo.modificarObjeto(proveedor);
                    }else {
                        Util.mostrarMensajeError("No es posible modificar el proveedor .\n" +
                                "Compruebe que los campos contengan el tipo de dato requerido.");
                    }
                    listarProveedores();
                } else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
                break;
            case "Eliminar proveedor":
                if (vista.listProveedores.getSelectedValue()!=null){
                    modelo.eliminarObjeto(vista.listProveedores.getSelectedValue());
                    listarProveedores();
                    limpiarCamposProveedor();
                    break;
                }else {
                    Util.mostrarMensajeError("No hay ningún elemento seleccionado.");
                }
        }
    }

    /**
     * Metodo que se utiliza para buscar lo que deseas buscar
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscarProducto) {
            listarProductosBusqueda(modelo.getProductos(vista.txtBuscarProducto.getText()));
            if (vista.txtBuscarProducto.getText().isEmpty()) {
                vista.dlmProductosBusqueda.clear();
            }
        }else if (e.getSource() == vista.txtBuscarEmpleado) {
            listarEmpleadosBusqueda(modelo.getEmpleados(vista.txtBuscarEmpleado.getText()));
            if (vista.txtBuscarEmpleado.getText().isEmpty()) {
                vista.dlmEmpleadosBusqueda.clear();
            }
        }else if (e.getSource()== vista.txtBuscarProveedor){
            listarProveedorBusqueda(modelo.getProveedores(vista.txtBuscarProveedor.getText()));
            if (vista.txtBuscarProveedor.getText().isEmpty()){
                vista.dlmProveedoresBusqueda.clear();
            }
        }
    }

    /**
     * Metodo que nos mostrara los datos al seleccionar lo que queremos ver
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()){
            if (vista.listProductos.getSelectedValue() != null){
                Producto producto = vista.listProductos.getSelectedValue();
                vista.txtNombreProducto.setText(producto.getNombre());
                vista.txtOrigenProducto.setText(producto.getOrigen());
                vista.txtPrecioProducto.setText(String.valueOf(producto.getPrecio()));
            }
        }else if (e.getValueIsAdjusting()){
            if (vista.listEmpleados.getSelectedValue() != null){
                Empleado empleado = vista.listEmpleados.getSelectedValue();
                vista.txtNombreEmpleado.setText(empleado.getNombre());
                vista.txtApellidosEmpleado.setText(empleado.getApellidos());
                vista.fecha_nacimiento.setDate(empleado.getFecha_nacimiento());
            }
        }else if (e.getValueIsAdjusting()){
            if (vista.listProveedores.getSelectedValue()!=null){
                Proveedor proveedor= vista.listProveedores.getSelectedValue();
                vista.txtProveedor.setText(proveedor.getProveedor());
            }
        }

    }
    private boolean comprobarCamposProveedor() {
        return !vista.txtProveedor.getText().isEmpty();

    }

    private boolean comprobarCamposEmpleado() {
        return !vista.txtNombreEmpleado.getText().isEmpty() &&
                !vista.txtApellidosEmpleado.getText().isEmpty() &&
                !vista.fecha_nacimiento.getText().isEmpty();
    }

    private boolean comprobarCamposProducto() {
        return !vista.txtNombreProducto.getText().isEmpty() &&
                !vista.txtOrigenProducto.getText().isEmpty()&&
                !vista.txtPrecioProducto.getText().isEmpty() &&
                comprobarFloat(vista.txtPrecioProducto.getText());
    }
    private boolean comprobarFloat(String text) {
        try {
            Float.parseFloat(text);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

    private void listarProveedorBusqueda(ArrayList<Proveedor> lista) {
        vista.dlmProveedoresBusqueda.clear();
        for (Proveedor proveedor : lista) {
            vista.dlmProveedoresBusqueda.addElement(proveedor);
        }
    }

    private void listarEmpleadosBusqueda(ArrayList<Empleado> lista) {
        vista.dlmEmpleadosBusqueda.clear();
        for (Empleado empleado : lista) {
            vista.dlmEmpleadosBusqueda.addElement(empleado);
        }
    }

    private void listarProductosBusqueda(ArrayList<Producto>lista) {
        vista.dlmProductosBusqueda.clear();
        for (Producto producto : lista){
           vista.dlmProductosBusqueda.addElement(producto);
        }
    }

    private void limpiarCamposProveedor() {
        vista.txtProveedor.setText("");
        vista.txtBuscarProveedor.setText("");

    }

    private void limpiarCamposEmpleado() {
        vista.txtNombreEmpleado.setText("");
        vista.txtApellidosEmpleado.setText("");
        vista.fecha_nacimiento.clear();
        vista.txtBuscarEmpleado.setText("");
    }

    private void limpiarCamposProducto() {
        vista.txtNombreProducto.setText("");
        vista.txtOrigenProducto.setText("");
        vista.txtPrecioProducto.setText("");
        vista.txtBuscarProducto.setText("");

    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }


}
