package com.tatiana.mongo.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.tatiana.mongo.base.Empleado;
import com.tatiana.mongo.base.Producto;
import com.tatiana.mongo.base.Proveedor;

import javax.swing.*;
import java.awt.*;

/**
 * Clase vista
 */
public class Vista extends JFrame {
    private JPanel panelPrincipal;
    JTabbedPane tabbedPane1;


    // Productos//

    JTextField txtNombreProducto;
    JTextField txtOrigenProducto;
    JTextField txtPrecioProducto;

    JList<Producto> listProductos;
    JButton btnAnnadirProducto;
    JButton btnModificarProducto;
    JButton btnEliminarProducto;

    JTextField txtBuscarProducto;
    JList<Producto> listBusquedaProducto;

    //Empleados//

    JTextField txtNombreEmpleado;
    JTextField txtApellidosEmpleado;
    JTextField txtCodigoEmpleado;
    DatePicker fecha_nacimiento;

    JList<Empleado> listEmpleados;
    JButton btnAnnadirEmpleado;
    JButton btnModificarEmpleado;
    JButton btnEliminarEmpleado;

    JTextField txtBuscarEmpleado;
    JList<Empleado> listBusquedaEmpleado;

    //Proveedores//

    JList<Proveedor>listProveedores;
    JButton btnModificarProveedor;
    JButton btnEliminarProveedor;
    JTextField txtProveedor;
    JButton btnAnnadirProveedor;

    JTextField txtBuscarProveedor;
    JList<Proveedor> listBusquedaProveedor;

    //Modelos//

    DefaultListModel<Producto>dlmProductos;
    DefaultListModel<Empleado>dlmEmpleados;
    DefaultListModel<Proveedor>dlmProveedores;

    DefaultListModel<Producto> dlmProductosBusqueda;
    DefaultListModel<Empleado>dlmEmpleadosBusqueda;
    DefaultListModel<Proveedor> dlmProveedoresBusqueda;

    //MENU//

    JMenuItem itemConectar;
    JMenuItem itemSalir;

    /**
     * Constructor de vista
     */
    public Vista(){
        setTitle("Fruteria - <SIN CONEXION>");
        setContentPane(panelPrincipal);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(900, 650));
        setResizable(false);
        pack();
        setVisible(true);

        inicializarModelos();
        inicializarMenu();
    }

    /**
     * Metodo que se encarga de inicializar el menu
     */

    private void inicializarMenu() {

        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("conexion");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(menuArchivo);

        setJMenuBar(menuBar);
    }

    /**
     * Metodo que se encarga de inicializarModelos
     */
    private void inicializarModelos() {
        dlmProductos = new DefaultListModel<>();
        listProductos.setModel(dlmProductos);

        dlmEmpleados = new DefaultListModel<>();
        listEmpleados.setModel(dlmEmpleados);

        dlmProveedores= new DefaultListModel<>();
        listProveedores.setModel(dlmProveedores);

        dlmProductosBusqueda= new DefaultListModel<Producto>();
        listBusquedaProducto.setModel(dlmProductosBusqueda);

        dlmEmpleadosBusqueda= new DefaultListModel<Empleado>();
        listBusquedaEmpleado.setModel(dlmEmpleadosBusqueda);

        dlmProveedoresBusqueda= new DefaultListModel<Proveedor>();
        listBusquedaProveedor.setModel(dlmProveedoresBusqueda);


    }



}
